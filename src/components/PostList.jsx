import PostItem from "./PostItem";

function PostList(props) {
  /*
  props: {
    list: [{...},{...},...]
  }
  */
  const listOfPosts = props.list;

  // pour afficher une liste d'éléments dans le JSX : map !
  return (
    <>
      <ul>
        {
          listOfPosts.map((eachPost) => {
            return (
              <PostItem
                title={eachPost.title}
                body={eachPost.body}
              />
            )
          })
        }
      </ul>
    </>
  );
}

export default PostList;
