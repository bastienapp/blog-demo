import axios from "axios";
import { useEffect, useState } from "react";
import PostList from "./components/PostList";

function App() {
  /*
    une variable existe dans son bloc et les blocs enfants
  */
  const [postList, setPostList] = useState([]); // null, 0, "", false

  /*
   useEffect : c'est pour faire une action sur le cycle de vie du composant

   - montage (création) du composant (ici c'est App) []
   - mise à jour du composant [state1, state2, ...]
   - démontage (destruction) du composant (return dans le useEffect)
  */
  useEffect(() => {
    // appeler l'api pour récupérer une image de chat aléatoire
    axios.get("https://jsonplaceholder.typicode.com/posts").then((response) => {
      // quand j'ai changé l'API
      // envoyer les données à mon composant PostList
      setPostList(response.data);
    });

    // ne se lance qu'au montage (à la création) du composant
  }, []);

  // useEffect
  // il faut des crochets quelque part ?

  // le JSX
  return (
    <>
      <PostList list={postList} />
    </>
  );
}

export default App;
